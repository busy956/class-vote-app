// 파일에서 스코프로 함수를 빼오는 것 (스코프 처리)
import {configureStore} from '@reduxjs/toolkit'
// 불러온 것의 이름을 바꿔도 상관없다(달라고 하지 않으면 default로 주는 것을 가져온다)
// { } 처리를 하면 이 파일안에 voteReducer라는 함수가 없기 때문에 문제가 생긴다
import voteReducer from './features/vote/voteSlice'

export default configureStore({
    reducer: {
        // voteSlice라고 하면 헷갈리니까 Reducer로 통일했다
        // key 이름은 추적할 수 있도록 이름을 모듈이랑 통일한다
        // layout에서 store를 가져와서 쓰기 때문에 이름이 이상하면 추적하기 힘들다
        vote: voteReducer,
    },
});