// 폴더는 정리용이고 중요한것은 import해서 Slice를 만들겠다 하는 것
// 이걸 import해줘야 slice를 만드는 함수를 호출할 수 있다
import { createSlice } from '@reduxjs/toolkit';

// init을 보면 초기화라고 생각한다
// key : value
// 관리할 State(값)인데 초기값을 넣어둔 것
// 변할 수도 있고 변하지 않게 할 수도 있다
const initialState = {
    students: ['김길동', '홍길동', '나길동'],
    questions: [
        { kind: 'GOOD', title: '책상이 가장 깨끗한 학생은?' },
        { kind: 'BAD', title: '책상이 가장 더러운 학생은?' },
        { kind: 'GOOD', title: '주변이 가장 깨끗한 학생은?' },
        { kind: 'BAD', title: '옷이 가장 더러운 학생은?' },
        { kind: 'GOOD', title: '가방이 가장 깨끗한 학생은?' }
    ],
    votes: {},
    currentQuestionIndex: 0,
    selections: {},
};

// 파일 이름과 같아야 찾을 수 있다
// createSlice한 결과물이 voteSlice에 담긴다
const voteSlice = createSlice({
    // 찾기 쉽게 slice 앞 단어랑 같게 한다
    name: 'vote',
    // 이자리에는 <State>가 들어간다
    // 그냥 위에 있는 거 통째로 넣어도 되는데 코드가 지저분해지니까...
    // 형식을 보면 클로저인것을 알 수 있다
    initialState,
    // toolkit을 써서 단순화되었다(쓰기 편해졌다)
    // 모든 행위 자체는 reducer만 한다고 생각한다
    // 행위를 하는 얘는 2명 (select, nextQuestion)
    // reducers는 최상위 결과물
    reducers: {
        // 앞쪽의 state는 주입한 적이 없다
        // 주입한 적이 없는데 적혀있다? 콜백함수
        // 객체안에 객체(select, nextQuestion)를 담은건데 그 객체가 함수인것뿐

        select: (state, action) => {
            const { student, question } = action.payload;
            // 여기서 state는 initialState이고 이거의 selections
            if (!state.selections[student]) {
                state.selections[student] = { GOOD: [], BAD: [] };
            }
            // push 추가한다
            // 값을 조작하기 위한 수식은 누구껀지 알 수 없지만
            // 중앙상태관리에 값을 넣으면서 값을 변경할 함수도 같이 넣는다
            // 이 값을 바꾸고 싶어? 이 함수 가져다가 써
            // 이러면 페이지에서 로직을 구현할 필요가 없어서 버그가 나면 slice에서 찾으면 된다
            // 필요한 페이지에서 이 함수 불러다가 쓰면 되니까 보일러 플레이트 함수가 나오지 않는다
            state.selections[student][question.kind].push(question);
        },
        nextQuestion: (state) => {
            for (const student in state.selections) {
                if (!state.votes[student]) {
                    state.votes[student] = { GOOD: 0, BAD: 0 };
                }
                state.votes[student].GOOD += state.selections[student].GOOD.length;v
                state.votes[student].BAD += state.selections[student].BAD.length;
            }
            state.selections = {};
            state.currentQuestionIndex++;
        },
    },
});

// 빼내겠다 voteSlice.actions를
// actions는 createSlice안에 구현한것이므로 reducer안에 actions를 구현한 것(select랑 nextQuestion)
// { }모양인건 actions가 객체니까 ( [ ]는 함수겠지? )
export const { select, nextQuestion } = voteSlice.actions;

// 한 파일에서 두번 뺄 수 있다
// 그래도 기본적으로 빼내는 것은 이거
// 달라고 하지 않아도 기본적으로 준다
export default voteSlice.reducer;

// reducer는 createSlice를 실행한 결과
// .이 있다는 것은 객체
// createSlice가 실행되면 이대로 객체 만들어줘
// 기본값을 initialState를 가지고 initialState를 조작할 수 있는 메서드 select, nextQuestion을 가지고
// createSlice의 결과값을 객체이다