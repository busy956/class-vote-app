'use client'
// hook을 쓸려면 use client를 써줘야 한다

// redux에서 제공하는 hook을 가지고 왔다
import {useSelector, useDispatch} from 'react-redux';
// voteSlice에서 export로 빼내지 않으면
import {select, nextQuestion} from '@/lib/features/vote/voteSlice';
import { useRouter } from 'next/navigation';
import { useState } from 'react';

export default function Home() {
    const dispatch = useDispatch();
    const router = useRouter();
    const { students, questions, votes, currentQuestionIndex } = useSelector((state) => state.vote);
    const [selected, setSelected] = useState({});

    const handleSelect = (student, question) => {
        dispatch(select({ student, question }));
        setSelected({ ...selected, [student]: !selected[student] });
    };

    const handleNext = () => {
        if (currentQuestionIndex < questions.length - 1) {
            dispatch(nextQuestion());
            setSelected({});
        } else {
            router.push('/result');
        }
    };

    const question = questions[currentQuestionIndex];

    return (
        <div>
            <h2>{question.title}</h2>
            {students.map((student, j) => (
                <div key={j}>
                    <input type="checkbox" checked={selected[student] || false} onChange={() => handleSelect(student, question)} />
                    <label>{student}</label>
                </div>
            ))}
            <button onClick={handleNext}>다음</button>
        </div>
    );
}
