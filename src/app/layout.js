'use client'
// use client를 metadata를 쓸 수 없다
// metadata는 애초에 중요한 바뀌지 않는 파일
// layout은 항상 밖을 감싸고 있기 때문에 바뀌지 않는 파일이지만
// use client로 안쪽에 있는 파일입니다 라고 하는 순간 충돌이 나서 쓸 수가 없다

import {Provider} from "react-redux";
import {Inter} from "next/font/google";
import "./globals.css";
import store from "@/lib/store";

const inter = Inter({subsets: ["latin"]});

export default function RootLayout({children}) {
    return (
        <html lang="en">
        <Provider store={store}>
            <body className={inter.className}>{children}</body>
        </Provider>
        </html>
    );
}
